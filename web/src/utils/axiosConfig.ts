import axios from 'axios';

const baseURL = 'http://localhost:8000';
export const TOKEN = 'project-zai-token';

const api = axios.create({
  baseURL,
  timeout: 60000,
});

api.interceptors.request.use((config) => {
  // if token is in local storage set it to request header

  if (localStorage.getItem(TOKEN)) {
    const value = localStorage.getItem(TOKEN);
    config.headers['Authorization'] = 'Token ' + value;
  }
  return config;
});
export default api;
