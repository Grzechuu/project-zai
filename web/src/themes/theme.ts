import { deepMerge } from 'grommet/utils';
import { grommet } from 'grommet';

export const customTheme = deepMerge(grommet, {
	global: {
		font: {
			family: 'Roboto',
		},
	},
});
