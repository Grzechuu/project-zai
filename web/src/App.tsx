import React from 'react';
import { Provider } from 'react-redux';
import store from 'redux/store';
import { Switch, Router } from 'react-router-dom';
import { NotAuthorizedRoute, PrivateRoute } from 'utils/Routes';
import { Grommet } from 'grommet';
import { customTheme } from 'themes/theme';
import './App.scss';
import Main from 'views/app/Main';
import Login from 'views/login/Login';
import DayPage from 'views/calendar/DayPage';
import history from 'utils/history';
import Register from 'views/register/Register';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Grommet full theme={customTheme}>
          <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} autoClose={5000} />
          <Switch>
            <NotAuthorizedRoute exact path="/login">
              <Login />
            </NotAuthorizedRoute>
            <NotAuthorizedRoute exact path="/register">
              <Register />
            </NotAuthorizedRoute>
            {/* //TODO WHEN AUTHORIZATION IS WORKING CHANGE TO PRIVATE ROUTE */}
            <PrivateRoute path={['/']}>
              <Main />
            </PrivateRoute>
          </Switch>
        </Grommet>
      </Router>
    </Provider>
  );
};

export default App;
