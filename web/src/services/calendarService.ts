import { Subject } from 'rxjs';

interface Calendar {
	date: Date;
}

const subject = new Subject<Calendar>();

export const calendarService = {
	increment: (value: Date) => subject.next({ date: value }),
	decrement: (value: Date) => subject.next({ date: value }),
	getDate: () => subject.asObservable(),
	unsubscribe: () => subject.unsubscribe(),
};
