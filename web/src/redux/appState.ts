import SecurityState from "./security/state";

export default interface AppState {
    security: SecurityState;
}