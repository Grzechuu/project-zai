import {combineReducers, Reducer} from 'redux';
import {securityReducer} from "./security/reducers";

const reducers: Reducer = combineReducers({
    security: securityReducer,
});

export default reducers;