import {actionTypes} from "./actions";
import SecurityState from "./state";

const initialState: SecurityState = {
    username: '',
    password: '',
    isLoading: false,
};

export function securityReducer(state: SecurityState = initialState, action: any) {
    switch (action.type) {
        case actionTypes.SET_CURRENT_USER:
            return {...state, username: action.payload};
        default:
            return state;
    }
}