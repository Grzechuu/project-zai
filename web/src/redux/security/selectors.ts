import AppState from 'redux/appState';

const currentUserRole = (state: AppState): string | null => {
    return state.security.username;
};