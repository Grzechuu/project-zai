export default interface SecurityState {
    username: string;
    password: string;
    isLoading: boolean;
}