import { createAction, ActionType } from 'typesafe-actions';
import { LoginResponse, handleLogin } from 'api/security';
import Jwt from './models/Jwt';


export enum actionTypes {
    SET_CURRENT_USER = 'user/SET_CURRENT_USER',
}

export const setCurrentUser = createAction(actionTypes.SET_CURRENT_USER)<LoginResponse>();