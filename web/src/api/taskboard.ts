import api from 'utils/axiosConfig';

export interface CreateTaskboardRequest {
  title: string;
  description: string;
}

export const handleCreateTaskBoard = async (props: CreateTaskboardRequest): Promise<any> => {
  const { title, description } = props;
  try {
    const response = await api.post<CreateTaskboardRequest>('/api/taskboard/', { title, description });
    return response;
  } catch (err) {
    return err.response;
  }
};

export const handleGetListTaskBoard = async (props?: any): Promise<any> => {
  try {
    const response = await api.get('/api/taskboard/');
    return response;
  } catch (err) {
    return err.response;
  }
};

export interface CreateTaskRequest {
  taskboard: string;
  dateFrom: string;
  dateTo: string;
  name: string;
  description: string;
}

export const handleCreateTask = async (data: CreateTaskRequest): Promise<any> => {
  // const { boardId: taskboard, dateFrom, dateTo, taskName: name, description } = data;
  try {
    const response = await api.post(`/api/taskboard/${data.taskboard}/task/`, data);
    return response;
  } catch (err) {
    return err.response;
  }
};

interface QueryParams {
  day: string | number;
  month: string | number;
  year: string | number;
}

export const fetchTasks = async (boardId: string | number, queryParams?: QueryParams): Promise<any> => {
  try {
    const response = await api.get(`/api/taskboard/${boardId}/task/`, { data: null, params: queryParams });
    return response;
  } catch (err) {
    return err.response;
  }
};

export interface DeleteTaskRequest {
  boardId: string | number;
  taskId: string | number;
}

export const handleDeleteTask = async (data: DeleteTaskRequest): Promise<any> => {
  try {
    const response = await api.delete(`/api/taskboard/${data.boardId}/task/${data.taskId}`);
    return response;
  } catch (err) {
    return err.response;
  }
};

export interface DeleteBoardRequest {
  boardId: string | number;
}

export const handleDeleteBoard = async (data: DeleteBoardRequest): Promise<any> => {
  try {
    const response = await api.delete(`/api/taskboard/${data.boardId}/`);
    return response;
  } catch (err) {
    return err.response;
  }
};
