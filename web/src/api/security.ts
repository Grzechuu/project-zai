import api, { TOKEN } from 'utils/axiosConfig';
import history from 'utils/history';
import { AxiosResponse, AxiosError } from 'axios';

export interface LoginResponse {
  token: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface RegisterRequest {
  email: string;
  password: string;
}

export const handleLogin = async (props: LoginRequest): Promise<AxiosResponse> => {
  const { email, password } = props;
  try {
    const response = await api.post<LoginResponse>('/api/auth/', { email, password });
    localStorage.setItem(TOKEN, response.data.token);
    return response;
  } catch (err) {
    return err.response;
  }
};

export const handleLogout = (): boolean => {
  try {
    localStorage.removeItem(TOKEN);
    history.push('/Login');
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const handleRegister = async (props: RegisterRequest): Promise<AxiosResponse> => {
  const { email, password } = props;
  try {
    const response = await api.post<any>('/api/account/', { email, password });
    return response;
  } catch (err) {
    return err.response;
  }
};
