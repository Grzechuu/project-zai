import React from 'react';
import RegisterForm from './forms/RegisterForm';
import { Box } from 'grommet';

const Register: React.FC = () => {
  return (
    <Box align="center" pad="medium" justify="center" height="100%">
      <RegisterForm />
    </Box>
  );
};

export default Register;
