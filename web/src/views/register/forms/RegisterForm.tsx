import React, { useCallback, useState } from 'react';
import { useFormik } from 'formik';
import { handleRegister } from 'api/security';
import { Box, Grid, TextInput, Button } from 'grommet';
import history from 'utils/history';
import { toast } from 'react-toastify';
import * as yup from 'yup';
import { FormError } from 'views/app/components/FormError';
import { ClipLoader } from 'react-spinners';

const RegisterSchema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required('E-mail jest wymagany'),
  password: yup.string().required('Haslo jest wymagane'),
  passwordConfirm: yup
    .string()
    .oneOf([yup.ref('password'), null])
    .required('Hasla nie sa identyczne'),
});

const RegisterForm: React.FC = () => {
  const [loading, setLoading] = useState(false);

  const handleLogin = useCallback(() => {
    history.push('/login');
  }, []);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      passwordConfirm: '',
      apiError: '',
    },
    onSubmit: async () => {
      setLoading(true);
      const response = await handleRegister({ email: formik.values.email, password: formik.values.password });
      if (response.status === 200) {
        formik.resetForm();
        toast.success('Konto zostalo stworzone.');
      } else {
        const { errors, ...rest } = response.data;
        formik.setErrors(rest);
        formik.setFieldError('apiErrors', errors);
        toast.error('Wystąpił problem');
      }
      setLoading(false);
    },
    enableReinitialize: true,
    validateOnBlur: false,
    validateOnChange: false,
    validationSchema: RegisterSchema,
  });
  return (
    <Box width={'70%'}>
      <form onSubmit={formik.handleSubmit}>
        <Grid rows={'auto'} gap={'1rem'}>
          <TextInput
            name={'email'}
            onChange={formik.handleChange}
            value={formik.values.email}
            size={'xlarge'}
            placeholder={'E-mail'}
          />
          {formik.errors.email && <FormError message={formik.errors.email} />}
          <TextInput
            name={'password'}
            onChange={formik.handleChange}
            value={formik.values.password}
            size={'xlarge'}
            placeholder={'Haslo'}
            type={'password'}
          />
          {formik.errors.password && <FormError message={formik.errors.password} />}
          <TextInput
            name={'passwordConfirm'}
            onChange={formik.handleChange}
            value={formik.values.passwordConfirm}
            size={'xlarge'}
            placeholder={'Potwierdz haslo'}
            type={'password'}
          />
          {formik.errors.passwordConfirm && <FormError message={formik.errors.passwordConfirm} />}
          {formik.errors.apiError && <FormError message={formik.errors.apiError} />}
          {!loading && <Button label={'Zaloguj'} type="submit" />}
          {loading && (
            <Box justify="center" align="center">
              <Button icon={<ClipLoader />} disabled />
            </Box>
          )}
          <span className="aligned-end">
            Masz konto,
            <span className="underlined" onClick={handleLogin}>
              zaloguj sie
            </span>
          </span>
        </Grid>
      </form>
    </Box>
  );
};

export default RegisterForm;
