import React, { useCallback, useState } from 'react';
import { useFormik } from 'formik';
import { TextInput, Grid, Button, Box } from 'grommet';
import { handleLogin } from 'api/security';
import history from 'utils/history';
import * as yup from 'yup';
import { ClipLoader } from 'react-spinners';
import { FormError } from 'views/app/components/FormError';

const LoginSchema = yup.object().shape({
  email: yup
    .string()
    .email('Nieprawidłowy adres email')
    .required('E-mail jest wymagany'),
  password: yup.string().required('Haslo jest wymagane'),
});

const LoginForm: React.FC = () => {
  const handleRegister = useCallback((): void => {
    history.push('/register');
  }, []);

  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      apiErrors: '',
    },
    validateOnChange: false,
    validateOnBlur: false,
    enableReinitialize: true,
    validationSchema: LoginSchema,
    onSubmit: async (values) => {
      setLoading(true);
      const response = await handleLogin(values);
      if (response.status === 200) {
        history.push('/');
      } else {
        formik.setFieldError('apiErrors', response.data.errors);
      }
      setLoading(false);
    },
  });
  return (
    <Box width={'70%'}>
      <form onSubmit={formik.handleSubmit}>
        <Grid rows={'auto'} gap={'1rem'}>
          <TextInput
            name={'email'}
            onChange={formik.handleChange}
            value={formik.values.email}
            size={'xlarge'}
            placeholder={'E-mail'}
          />

          {formik.errors.email && <FormError message={formik.errors.email} />}
          <TextInput
            name={'password'}
            onChange={formik.handleChange}
            value={formik.values.password}
            size={'xlarge'}
            placeholder={'Password'}
            type={'password'}
          />
          {formik.errors.password && <FormError message={formik.errors.password} />}
          {formik.errors.apiErrors && <FormError message={formik.errors.apiErrors[0]} />}
          {!loading && <Button label={'Zaloguj'} type="submit" />}
          {loading && (
            <Box justify="center" align="center">
              <Button icon={<ClipLoader />} disabled />
            </Box>
          )}
          <span className="aligned-end">
            Nie masz konta,
            <span className="underlined" onClick={handleRegister}>
              zarejestruj sie
            </span>
          </span>
        </Grid>
      </form>
    </Box>
  );
};

export default LoginForm;
