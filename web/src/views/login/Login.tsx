import React from 'react';
import LoginForm from './components/forms/LoginForm';
import { Box } from 'grommet';

const Login: React.FC = () => {
  return (
    <Box align="center" pad="medium" justify="center" height="100%">
      <LoginForm />
    </Box>
  );
};

export default Login;
