import React, { useState } from 'react';
import { Text, Button, Grid, Box } from 'grommet';
import { handleDeleteTask } from 'api/taskboard';
import { toast } from 'react-toastify';
import { ClipLoader } from 'react-spinners';

interface Props {
  boardId: string | number;
  deleteData: {
    name: string;
    taskId: number | string;
  };
  toggle: () => void;
  handleRefresh: () => Promise<void>;
}

const DeleteForm: React.FC<Props> = (props) => {
  const [loading, setLoading] = useState(false);
  const {
    toggle,
    boardId,
    deleteData: { name, taskId },
    handleRefresh,
  } = props;

  const handleDelete = async (): Promise<void> => {
    const response = await handleDeleteTask({ boardId, taskId });
    setLoading(true);
    if (response.status === 204) {
      toast.success('Usunięto zadanie.');
      await handleRefresh();
      setLoading(false);
      toggle();
    } else {
      toast.warn('Coś poszło nie tak.');
      setLoading(false);
    }
  };

  return (
    <Grid
      style={{
        padding: '2rem',
      }}
      rows={['1fr', '5rem', 'auto']}
      gap="1rem"
    >
      <Text>Czy na pewno chcesz usunąć zadanie</Text>
      <Text textAlign="center" size="xlarge">
        {name}
      </Text>
      <Grid columns={['1fr', 'auto']} justify="end" gap="small">
        <Button label={'Anuluj'} onClick={toggle} disabled={loading} />
        <Button
          label={
            loading ? (
              <Box direction="row" gap="small" align="center" justify="center">
                <ClipLoader />
              </Box>
            ) : (
              'Usuń'
            )
          }
          onClick={handleDelete}
          disabled={loading}
        />
      </Grid>
    </Grid>
  );
};

export default DeleteForm;
