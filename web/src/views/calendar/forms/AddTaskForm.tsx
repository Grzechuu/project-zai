import React, { useState } from 'react';
import { TextInput, Grid, Button, Box, Text, TextArea } from 'grommet';
import { useFormik } from 'formik';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { toast } from 'react-toastify';
import { setHours, setMinutes, getDate, formatISO } from 'date-fns';
import { useParams } from 'react-router';
import { handleCreateTask } from 'api/taskboard';
import { ClipLoader } from 'react-spinners';
import { FormError } from 'views/app/components/FormError';

interface Props {
  toggle: () => void;
  handleRefresh: () => Promise<void>;
}

const AddTaskForm = (props: Props) => {
  const [loading, setLoading] = useState(false);
  const { toggle, handleRefresh } = props;
  const { boardId, day, month, year } = useParams();

  const formik = useFormik({
    initialValues: {
      dateFrom: new Date(),
      dateTo: new Date(),
      name: '',
      description: '',
    },
    onSubmit: async (values) => {
      const { name, description } = values;
      const dateFrom = formatISO(values.dateFrom);
      const dateTo = formatISO(values.dateTo);
      console.log(values, dateFrom, dateTo, 'ASDASDASasdasdasS');
      const taskboard = boardId;
      if (taskboard) {
        setLoading(true);
        const response = await handleCreateTask({ taskboard, name, dateFrom, dateTo, description });
        if (response.status === 201) {
          toast.success('Udało dodać się zadanie.');
          setLoading(false);
          await handleRefresh();
          toggle();
        } else if (response.status == 400) {
          const { errors, ...rest } = response.data;
          formik.setErrors(rest);
          formik.setFieldError('apiErrors', errors);
          setLoading(false);
        } else {
          toast.warn('Nie Udało dodać się zadania!');
          setLoading(false);
        }
      }
    },
  });
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(new Date());

  return (
    <Box width={'35vw'}>
      <form onSubmit={formik.handleSubmit} style={{ height: '100%' }}>
        <Grid rows={['auto auto auto auto 1fr']} gap={'small'} style={{ padding: '2rem', height: '100%' }}>
          <Text size="xlarge" textAlign="center" style={{ marginBottom: '1rem' }}>
            Dodawanie zadania
          </Text>
          <TextInput
            name={'name'}
            onChange={formik.handleChange}
            value={formik.values.name}
            size={'xlarge'}
            placeholder={'Nazwa zadania'}
          />
          {formik.errors.name && <FormError message={formik.errors.name} />}
          <TextArea
            name={'description'}
            value={formik.values.description}
            onChange={formik.handleChange}
            placeholder={'Dodaj opis...'}
            rows={5}
          />
          {formik.errors.description && <FormError message={formik.errors.description} />}
          <div style={{ display: 'grid', gridTemplateColumns: 'auto auto', gap: '1rem' }}>
            <div style={{ display: 'grid', gridAutoRows: 'auto' }}>
              <Text>Data od</Text>
              <DatePicker
                showTimeSelect
                selected={formik.values.dateFrom}
                startDate={startDate}
                onChange={(date: Date) => formik.setFieldValue('dateFrom', date)}
                selectsStart
                minDate={new Date()}
                endDate={endDate}
                dateFormat="MMMM d, yyyy h:mm aa"
                customInput={<TextInput name={'dateFrom'} onChange={formik.handleChange} />}
              />
              {formik.errors.dateFrom && <FormError message={`${formik.errors.dateFrom}`} />}
            </div>
            <div style={{ display: 'grid', gridAutoRows: 'auto' }}>
              <Text>Data do</Text>
              <DatePicker
                showTimeSelect
                dateFormat="MMMM d, yyyy h:mm aa"
                selected={formik.values.dateTo}
                onChange={(date: Date) => formik.setFieldValue('dateTo', date)}
                selectsEnd
                startDate={startDate}
                endDate={endDate}
                minDate={startDate}
                customInput={<TextInput name={'dateTo'} onChange={formik.handleChange} />}
              />
              {formik.errors.dateTo && <FormError message={`${formik.errors.dateTo}`} />}
            </div>
          </div>
          <div className="modal-actions">
            <Button label={'Zamknij'} onClick={toggle} disabled={loading} />
            <Button
              label={
                loading ? (
                  <Box direction="row" gap="small" align="center" justify="center">
                    <ClipLoader />
                  </Box>
                ) : (
                  'Dodaj'
                )
              }
              type={'submit'}
              disabled={loading}
            />
          </div>
        </Grid>
      </form>
    </Box>
  );
};

export default AddTaskForm;
