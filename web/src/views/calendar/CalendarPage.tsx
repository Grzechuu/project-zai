import React, { useState, useEffect } from 'react';
import DayPage from './DayPage';
import { PrivateRoute } from 'utils/Routes';
import CalendarPanel from './CalendarPanel';
import Calendar from './Calendar';
import AddTaskForm from './forms/AddTaskForm';
import Modal from 'views/app/components/Modal';
import { Grid } from 'grommet';
import { useParams } from 'react-router';
import { Switch } from 'react-router-dom';
import { fetchTasks } from 'api/taskboard';
import DeleteForm from './forms/DeleteForm';
import useToggler from 'utils/hooks/useToggler';

interface RouteParams {
  boardId: string;
  day?: string;
  month?: string;
  year?: string;
}

interface Task {
  id: string;
  name: string;
  description: string;
  dateFrom: string;
  dateTo: string;
}

const CalendarPage: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [formModal, toggleFormModal] = useToggler();
  const [deleteModal, toggleDeleteModal] = useToggler();
  const [loading, setLoading] = useState(false);
  const { boardId, day, month, year } = useParams<RouteParams>();
  const [deleteData, setDeleteData] = useState<{ name: string; taskId: string | number }>();

  const onDeleteClick = ({ name, taskId }: { name: string; taskId: number | string }) => {
    setDeleteData({ name, taskId });
    toggleDeleteModal();
  };

  const fetchData = async () => {
    setLoading(true);
    if (boardId && day && month && year) {
      const response = await fetchTasks(boardId, { day, month, year });
      setTasks(response.data);
    } else if (boardId) {
      const response = await fetchTasks(boardId);
      setTasks(response.data);
    }
    setLoading(false);
  };

  useEffect(() => {
    (async (): Promise<void> => {
      if (!day && !month && !year) {
        await fetchData();
      }
    })();
  }, [boardId, day, month, year]);

  return (
    <>
      <Grid
        columns={['1/4', 'auto']}
        rows={['auto']}
        areas={[
          { name: 'panel', start: [0, 0], end: [0, 0] },
          { name: 'calendar', start: [1, 0], end: [1, 0] },
        ]}
        style={{ height: '100%' }}
        gap="small"
      >
        <CalendarPanel toggle={toggleFormModal} tasks={tasks} loading={loading} onDeleteClick={onDeleteClick} />
        <Switch>
          <PrivateRoute exact path={'/boards/:boardId'}>
            <Calendar />
          </PrivateRoute>
          <PrivateRoute exact path="/boards/:boardId/d/:day/m/:month/y/:year">
            <DayPage setTasksPanel={setTasks} />
          </PrivateRoute>
        </Switch>
      </Grid>
      <Modal open={formModal} setOpen={toggleFormModal}>
        <AddTaskForm toggle={toggleFormModal} handleRefresh={fetchData} />
      </Modal>
      {deleteData && (
        <Modal open={deleteModal} setOpen={toggleDeleteModal}>
          <DeleteForm toggle={toggleDeleteModal} boardId={boardId} deleteData={deleteData} handleRefresh={fetchData} />
        </Modal>
      )}
    </>
  );
};

export default CalendarPage;
