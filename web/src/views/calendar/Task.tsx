import React from 'react';

interface Props {
  start: number | string;
  end: number | string;
}

const Task = (props: Props) => {
  const { start, end } = props;
  return (
    <div className="task">
      {start}
      <br />
      {end}
    </div>
  );
};

export default Task;
