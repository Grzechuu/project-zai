import React, { useEffect, useState, useCallback, useRef, useMemo } from 'react';
import { useParams } from 'react-router';
import CalendarPanel from './CalendarPanel';
import { format, getHours, getMinutes } from 'date-fns';
import { Grid } from 'grommet';
import Task from './Task';
import { Rnd } from 'react-rnd';
import { debounce } from 'lodash';
import { fetchTasks } from 'api/taskboard';

interface Task {
  id: string;
  name: string;
  description: string;
  dateFrom: string;
  dateTo: string;
}

interface TaskWithColor extends Task {
  color: string;
}

interface Props {
  setTasksPanel: (tasks: Task[]) => void;
}

const DayPage: React.FC<Props> = (props) => {
  const { setTasksPanel } = props;

  const [tasksWithColor, setTaskWithColor] = useState<TaskWithColor[]>([]);
  const [tasks, setTasks] = useState<Task[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {}, []);

  const [isCreating, setIsCreating] = useState(false);
  const [rowHeight, setRoWHeight] = useState(0);
  const [start, setStart] = useState<number>(0);
  const [end, setEnd] = useState<number>(0);
  const taskRef = useRef<any>();
  const [date, setDate] = useState(new Date());
  const emptyArray = Array(24).fill(0);
  //   const hoursArray = emptyArray.map((hour, index) => {
  //     const temp = hour + index;
  //     if (index < 10) {
  //       return '0' + temp + ':00';
  //     } else {
  //       return temp + ':00';
  //     }
  //   });

  const { boardId, day, month, year } = useParams();
  const fetchData = async () => {
    setLoading(true);
    if (boardId && day && month && year) {
      const response = await fetchTasks(boardId, { day, month, year });
      setTasksPanel(response.data);

      const randomBgColor = () => {
        const x = Math.floor(Math.random() * 256);
        const y = Math.floor(Math.random() * 256);
        const z = Math.floor(Math.random() * 256);
        const bgColor = 'rgb(' + x + ',' + y + ',' + z + ')';
        return bgColor;
      };

      const tasksWithColor = response.data.map((task: Task) => {
        const color = randomBgColor();
        return {
          color,
          ...task,
        };
      });
      setTaskWithColor(tasksWithColor);
    }
    setLoading(false);
  };
  useEffect(() => {
    setDate(new Date(`${year}-${month}-${day}`));
    (async () => {
      await fetchData();
    })();
  }, [day, month, year]);

  const countPosition = (dateFrom: string, dateTo: string): { start: number; end: number } => {
    const dFrom = new Date(dateFrom);
    const dTo = new Date(dateTo);
    const formatedDFrom = format(dFrom, 'yyyy-MM-dd');
    const formatedDTo = format(dTo, 'yyyy-MM-dd');
    const start =
      formatedDFrom !== format(date, 'yyyy-MM-dd') ? 0 : getHours(dFrom) + Math.round((getMinutes(dFrom) / 60) * 2) / 2;
    console.log(getHours(dTo), getHours(dFrom));
    let end;
    if (start !== 0) {
      end =
        formatedDTo !== format(date, 'yyyy-MM-dd')
          ? 24
          : getHours(dTo) - getHours(dFrom) + Math.round((getMinutes(dTo) / 60) * 2) / 2;
    } else {
      end =
        formatedDTo !== format(date, 'yyyy-MM-dd') ? 24 : getHours(dTo) + Math.round((getMinutes(dTo) / 60) * 2) / 2;
    }
    console.log(start, end, 'END');
    return { start, end };
  };

  const hoursArray = emptyArray.map((hour, index) => hour + index);
  const [open, setOpen] = useState(false);

  const toggle = (): void => {
    setOpen((prev) => !prev);
  };

  const setRowHeightOnResize = (): void => {
    const tile = document.querySelector(`.day`);
    const height = tile?.getBoundingClientRect().height;
    if (height) {
      setRoWHeight(height);
    }
  };

  useEffect(() => {
    setRowHeightOnResize();
    window.addEventListener('resize', setRowHeightOnResize);
    return (): void => window.removeEventListener('resize', setRowHeightOnResize);
  }, []);

  const setTaskHourRange = (position: { x: number; y: number; deltaY: number }): void => {
    const { deltaY } = position;
    setStart((prev) => prev + deltaY / rowHeight);
    setEnd((prev) => prev + deltaY / rowHeight);
  };

  const createNewTask = (e: any): void => {
    const hm = document.querySelector('.day__presentation');
    if (hm && !isCreating) {
      const rect = hm?.getBoundingClientRect();
      const offset = Math.ceil(rect.top);
      const value = Math.round(((e.clientY - offset - rowHeight / 2) / rowHeight) * 2) / 2;
      setStart(value);
      setEnd(value + 1);
      setIsCreating(true);
    }
  };

  const resizeTask = (delta: any, position: any, direction: any) => {
    const tmp = Math.round((delta.height / rowHeight) * 2) / 2;
    if (direction === 'top' && delta.height > 0) {
      setStart((prev) => prev - tmp);
    } else if (direction === 'top' && delta.height < 0) {
      setStart((prev) => prev + Math.abs(tmp));
    } else {
      setEnd((prev) => prev + tmp);
    }
  };

  return (
    <Grid onClick={createNewTask}>
      {format(date, 'dd/MM/yyyy')}
      <div className="day-container">
        {hoursArray.map((hour, index) => (
          <div className="day" key={hour} id={hour} style={{ gridRowStart: index + 1, gridRowEnd: index + 1 }}>
            <div className="day__hour" key={hour} id={hour}>
              <span>{hour}</span>
            </div>
          </div>
        ))}
        <div className="day__presentation">
          {tasksWithColor.map((task) => {
            const position = countPosition(task.dateFrom, task.dateTo);
            return (
              <Rnd
                key={task.id}
                default={{
                  x: 56,
                  y: (position.start + 0.5) * rowHeight,
                  width: '90%',
                  height: position.end === 0 ? rowHeight : rowHeight * (position.end - position.start),
                }}
                style={{
                  backgroundColor: task.color,
                  zIndex: 2,
                }}
                ref={taskRef}
                minHeight={Math.ceil(rowHeight / 2)}
                bounds="parent"
                dragAxis={'y'}
                resizeGrid={[rowHeight / 2, rowHeight / 2]}
                dragGrid={[rowHeight / 2, rowHeight / 2]}
                onDrag={(e, d) => setTaskHourRange(d)}
                onResizeStop={(e, d, r, delta, position) => resizeTask(delta, position, d)}
                className="task"
              >
                <span>{task.name}</span>
                {/* <Task start={task.dateFrom} end={task.dateTo} /> */}
              </Rnd>
            );
          })}
        </div>
      </div>
    </Grid>
  );
};
export default DayPage;
