import React from 'react';
import { Grid } from 'grommet';

import { CalendarHeader, CalendarBody } from 'views/calendar/components';

const Calendar: React.FC = () => {
  return (
    <Grid rows={['xxsmall', '1fr']} className="calendar" style={{ background: '#f2f2f2' }}>
      <CalendarHeader />
      <CalendarBody />
    </Grid>
  );
};
export default Calendar;
