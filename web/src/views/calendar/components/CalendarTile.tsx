import React from 'react';
import history from 'utils/history';
import { useLocation } from 'react-router-dom';
type Props = {
  day: number;
  month: number;
  year: number;
};

export const CalendarTile: React.FC<Props> = ({ day, month, year, children }) => {
  const location = useLocation();
  return (
    <div
      className="calendar-body__tile"
      onClick={(): void => history.push(`${location.pathname}/d/${day}/m/${month}/y/${year}`)}
    >
      {children}
    </div>
  );
};
