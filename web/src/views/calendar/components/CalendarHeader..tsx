import React, { useState, useEffect } from 'react';

import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import { Box } from 'grommet';

import { format, addMonths } from 'date-fns';
import pl from 'date-fns/locale/pl';

import { calendarService } from 'services/calendarService';

const CalendarHeader: React.FC = () => {
    const [date, setDate] = useState<Date>(new Date());
    useEffect(() => {
        const sub = calendarService.getDate().subscribe(value => {
            setDate(value.date);
        });
        return (): void => sub.unsubscribe()
    }, []);

    const incrementMonth = (): void => {
        calendarService.increment(addMonths(date, 1));
    };
    const decrementMonth = (): void => {
        calendarService.decrement(addMonths(date, -1));
    };

    return (
        <Box direction="row" justify="center" align="center">
            <FaAngleLeft onClick={decrementMonth} /> <FaAngleRight onClick={incrementMonth} />{' '}
            {format(date, 'LLLL yyyy', { locale: pl })}
        </Box>
    );
};

export default CalendarHeader;
