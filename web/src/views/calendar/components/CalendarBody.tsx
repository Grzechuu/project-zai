import React, { useEffect, useState } from 'react';
import { calendarService } from 'services/calendarService';
import {
  eachDayOfInterval,
  startOfMonth,
  endOfMonth,
  format,
  addDays,
  getISODay,
  getDate,
  getMonth,
  getYear,
} from 'date-fns';
import { Grid } from 'grommet';
import { CalendarTile } from './CalendarTile';

const color = '#3e3e3e33';
const monthColor = '#3e3e3e';

const CalendarBody: React.FC = () => {
  const [date, setDate] = useState<Date>(new Date());

  useEffect(() => {
    const sub = calendarService.getDate().subscribe((value) => {
      setDate(value.date);
    });
    return (): void => sub.unsubscribe();
  }, []);

  const firstDayOfMonth = startOfMonth(date);
  const lastDayOfMonth = endOfMonth(date);

  const numberOfFirstDayOfMonth = getISODay(firstDayOfMonth);

  const previousMonthDaysCount = numberOfFirstDayOfMonth - 1;

  let previousMonthDays: Date[] = [];

  if (previousMonthDaysCount !== 0) {
    previousMonthDays = eachDayOfInterval({
      start: addDays(firstDayOfMonth, -previousMonthDaysCount),
      end: addDays(firstDayOfMonth, -1),
    });
  }

  const currentMonthDays = eachDayOfInterval({ start: firstDayOfMonth, end: lastDayOfMonth });

  const nextMonthDaysCount = 42 - (previousMonthDays.length + currentMonthDays.length);

  const nextMonthDays = eachDayOfInterval({
    start: addDays(lastDayOfMonth, +1),
    end: addDays(lastDayOfMonth, nextMonthDaysCount),
  });

  const previousMonthDayWithColor = previousMonthDays.map((day) => {
    return { day, color };
  });

  const currentMonthDaysWithColor = currentMonthDays.map((day) => {
    return { day, color: monthColor };
  });

  const nextMonthDaysWithColor = nextMonthDays.map((day) => {
    return { day, color };
  });

  const allDays = [...previousMonthDayWithColor, ...currentMonthDaysWithColor, ...nextMonthDaysWithColor];

  return (
    <Grid
      rows={'auto'}
      columns={['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto']}
      className="calendar-body"
      gap={'0.5rem'}
    >
      {allDays.map((date, index) => {
        const day = getDate(date.day);
        const month = getMonth(date.day);
        const year = getYear(date.day);

        return (
          <CalendarTile key={index} day={day} month={month + 1} year={year}>
            <span style={{ color: date.color }}>{format(date.day, 'dd/MM/yyyy')}</span>
          </CalendarTile>
        );
      })}
    </Grid>
  );
};

export default CalendarBody;
