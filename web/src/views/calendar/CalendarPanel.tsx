import React from 'react';
import { Button, Grid, Box, Text } from 'grommet';
import { ClipLoader } from 'react-spinners';
import { format } from 'date-fns';
import { Trash } from 'grommet-icons';

interface Task {
  id: string;
  name: string;
  description: string;
  dateFrom: string;
  dateTo: string;
}

interface Props {
  toggle: () => void;
  tasks: Task[];
  loading: boolean;
  onDeleteClick: ({ name, taskId }: { name: string; taskId: string | number }) => void;
}

const CalendarPanel: React.FC<Props> = (props) => {
  const { toggle, tasks, loading, onDeleteClick } = props;
  return (
    <Box
      gridArea="panel"
      background="light-5"
      style={{
        padding: '1rem',
        overflowY: 'auto',
      }}
    >
      <Button primary color="accent-1" label={'Dodaj zadanie'} onClick={toggle} style={{ marginBottom: '1rem' }} />
      {!loading ? (
        tasks.length > 0 ? (
          tasks.map((item) => (
            <Grid
              key={item.id}
              style={{
                padding: '1rem',
                border: '1px solid #f2f2f2',
                backgroundColor: '#f2f2f2',
                borderRadius: '0.25rem',
                marginBottom: '1rem',
              }}
            >
              <Box justify="between" direction="row">
                <Text size="large">{item.name}</Text>
                <Trash color="red" onClick={(): void => onDeleteClick({ name: item.name, taskId: item.id })} />
              </Box>
              <Text>{format(new Date(item.dateFrom), 'dd-MM-yyyy HH:mm')}</Text>
              <Text>{format(new Date(item.dateTo), 'dd-MM-yyyy HH:mm')}</Text>
            </Grid>
          ))
        ) : (
          <Text size="large" textAlign="center">
            Brak zadań
          </Text>
        )
      ) : (
        <Box justify="center" align="center">
          <ClipLoader />
        </Box>
      )}
    </Box>
  );
};
export default CalendarPanel;
