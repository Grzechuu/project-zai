import React, { useState } from 'react';
import { Box, Heading } from 'grommet';
import { User } from 'grommet-icons';
import BoardItem from './components/BoardItem';
import DeleteForm from './forms/DeleteForm';
import Modal from 'views/app/components/Modal';
import useToggler from 'utils/hooks/useToggler';

interface TaskBoard {
  id: number;
  title: string;
  description: string;
}

interface Props {
  taskBoards: TaskBoard[];
  handleRefresh: () => Promise<void>;
}

const Boards: React.FC<Props> = (props) => {
  const [deleteData, setDeleteData] = useState<{ name: string; boardId: string | number }>();
  const [deleteModal, toggleDeleteModal] = useToggler();
  const { taskBoards, handleRefresh } = props;
  const onDeleteClick = ({ name, boardId }: { name: string; boardId: string | number }) => {
    setDeleteData({ name, boardId });
    toggleDeleteModal();
  };

  return (
    <>
      <Box background="neutral-3" gridArea="boards">
        <Heading level="3" margin="xsmall">
          <User />
          Twoje tablice
        </Heading>
        <Box wrap direction="row" pad="small">
          {taskBoards.map((taskBoard, index) => {
            return <BoardItem key={index} name={taskBoard.title} id={taskBoard.id} onDeleteClick={onDeleteClick} />;
          })}
        </Box>
      </Box>
      {deleteData && (
        <Modal open={deleteModal} setOpen={toggleDeleteModal}>
          <DeleteForm toggle={toggleDeleteModal} deleteData={deleteData} handleRefresh={handleRefresh} />
        </Modal>
      )}
    </>
  );
};
export default Boards;
