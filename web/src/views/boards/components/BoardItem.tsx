import React from 'react';
import { Box, Text } from 'grommet';
import history from 'utils/history';
import { Trash } from 'grommet-icons';

type Props = {
  name: string;
  id: number;
  onDeleteClick: ({ name, boardId }: { name: string; boardId: string | number }) => void;
};

const BoardItem: React.FC<Props> = ({ name, id, onDeleteClick }) => {
  const handleBoardClick = (e: any): void | boolean => {
    e.stopPropagation();
    if (e.target.getAttribute('id') === 'trash') {
      return false;
    }
    history.push(`/boards/${id}`);
  };

  return (
    <Box
      onClick={(e: any) => handleBoardClick(e)}
      wrap
      className="board-item"
      pad="small"
      width={'250px'}
      height={'150px'}
      direction="row-responsive"
      background={{
        image: 'url(https://support.content.office.net/pl-pl/media/c1633a4d-a445-4f6c-87fc-f41fc8f01b69.jpg)',
      }}
      // border={{ style: 'solid', size: 'small' }}
      margin={{ right: '10px', bottom: '10px' }}
      style={{ position: 'relative', overflow: 'hidden' }}
      title={name}
    >
      <span className="board-image-fade">
        <Text style={{ position: 'relative' }} truncate>
          {name}
        </Text>
        <Trash id="trash" color="red" onClick={(): void => onDeleteClick({ name, boardId: id })} />
      </span>
    </Box>
  );
};

export default BoardItem;
