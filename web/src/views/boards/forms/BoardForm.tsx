import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import { TextInput, Grid, Button, Box, TextArea, Text } from 'grommet';
import { FormError } from 'views/app/components/FormError';
import { handleCreateTaskBoard } from 'api/taskboard';
import { toast } from 'react-toastify';
import history from 'utils/history';
import { ClipLoader } from 'react-spinners';

interface Props {
  toggle: () => void;
  handleRefresh: () => void;
}

const BoardForm: React.FC<Props> = (props) => {
  const { toggle, handleRefresh } = props;

  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
    },
    validateOnChange: false,
    validateOnBlur: false,
    enableReinitialize: true,
    onSubmit: async (values) => {
      setLoading(true);
      const response = await handleCreateTaskBoard(values);
      if (response.status === 200 || response.status === 201) {
        toast.success('Utworzono nową tablicę!');
        handleRefresh();
        toggle();
        return;
      } else if (response.status == 400) {
        const { errors, ...rest } = response.data;
        formik.setErrors(rest);
        formik.setFieldError('apiErrors', errors);
      }
      setLoading(false);
    },
  });

  useEffect(() => {
    formik.resetForm();
  }, []);

  return (
    <Grid rows={['auto auto 1fr auto']} gap={'1rem'} style={{ padding: '3rem', minWidth: '25vw', minHeight: '30vh' }}>
      <form onSubmit={formik.handleSubmit}>
        <Text size="xlarge" style={{ marginBottom: '1rem' }} textAlign="center">
          Tworzenie nowej tablicy
        </Text>
        <TextInput
          name={'title'}
          value={formik.values.title}
          onChange={formik.handleChange}
          size={'xlarge'}
          placeholder={'Podaj tytuł'}
        />
        {formik.errors.title && <FormError message={formik.errors.title} />}
        <TextArea
          name={'description'}
          value={formik.values.description}
          onChange={formik.handleChange}
          placeholder={'Dodaj opis...'}
        />
        {formik.errors.description && <FormError message={formik.errors.description} />}
        <div className="modal-actions">
          <Button label={'Zamknij'} onClick={toggle} disabled={loading} />

          <Button
            disabled={loading}
            type={'submit'}
            label={
              loading ? (
                <Box direction="row" gap="small" align="center" justify="center">
                  <ClipLoader />
                </Box>
              ) : (
                'Utwórz'
              )
            }
          />
        </div>
      </form>
    </Grid>
  );
};

export default BoardForm;
