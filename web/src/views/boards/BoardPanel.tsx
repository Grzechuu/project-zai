import React from 'react';
import { Button, Box } from 'grommet';

interface Props {
  toggle: () => void;
}

const BoardPanel: React.FC<Props> = (props) => {
  const { toggle } = props;
  return (
    <Box gridArea="panel" background="light-5">
      <Button primary color="accent-1" label={'Utwórz nową tablicę'} onClick={toggle} />
    </Box>
  );
};
export default BoardPanel;
