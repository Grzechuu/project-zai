import React, { useState, useEffect } from 'react';
import { Grid } from 'grommet';

import BoardPanel from './BoardPanel';
import Boards from './Boards';
import { handleGetListTaskBoard } from 'api/taskboard';
import BoardForm from './forms/BoardForm';
import Modal from 'views/app/components/Modal';

interface Taskboard {
  id: number;
  title: string;
  description: string;
}
const BoardsPage: React.FC = () => {
  const [open, setOpen] = useState(false);

  const toggle = (): void => {
    setOpen((prev) => !prev);
  };

  const [loading, setLoading] = useState(true);
  const [taskBoards, setTaskBoards] = useState<Array<Taskboard>>([]);

  const handleRefresh = async (): Promise<void> => {
    setLoading(true);
    const response = await handleGetListTaskBoard();
    console.log(response);
    if (response.status === 200) {
      setTaskBoards(response.data);
    }
    setLoading(false);
  };

  useEffect(() => {
    (async (): Promise<void> => {
      await handleRefresh();
    })();
  }, []);

  return (
    <Grid
      columns={['1/4', 'auto']}
      rows={['auto']}
      areas={[
        { name: 'panel', start: [0, 0], end: [0, 0] },
        { name: 'boards', start: [1, 0], end: [1, 0] },
      ]}
      style={{ height: '100%' }}
      gap="small"
    >
      <BoardPanel toggle={toggle} />
      <Boards taskBoards={taskBoards} handleRefresh={handleRefresh} />
      <Modal open={open} setOpen={toggle}>
        <BoardForm toggle={toggle} handleRefresh={handleRefresh} />
      </Modal>
    </Grid>
  );
};

export default BoardsPage;
