import React from 'react';
import { Box, Grid, Text } from 'grommet';
import { PrivateRoute } from 'utils/Routes';
import DayPage from 'views/calendar/DayPage';
import { Switch } from 'react-router-dom';
import Navbar from './components/navbar';
import BoardsPage from 'views/boards/BoardsPage';
import CalendarPage from 'views/calendar/CalendarPage';

const Main: React.FC = () => {
  return (
    <Grid
      rows={['xsmall', '1fr']}
      columns={['1fr']}
      gap="small"
      areas={[
        { name: 'header', start: [0, 0], end: [0, 0] },
        // { name: 'nav', start: [1, 0], end: [0, 1] },
        { name: 'main', start: [0, 1], end: [0, 1] },
      ]}
      style={{ height: '100%' }}
    >
      <Navbar />
      <Box gridArea="main">
        <Switch>
          <PrivateRoute exact path={['/boards/:boardId/d/:day/m/:month/y/:year', '/boards/:boardId']}>
            <CalendarPage />
          </PrivateRoute>
          <PrivateRoute exact path={['/', '/boards']}>
            <BoardsPage />
          </PrivateRoute>
        </Switch>
      </Box>
    </Grid>
  );
};

export default Main;
