import React from 'react';

interface Props {
  message: string;
}

export const FormError = (props: Props) => {
  const { message } = props;
  return <span style={{ color: 'red' }}>{message}</span>;
};
