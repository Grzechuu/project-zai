import React from 'react';
import ReactDOM from 'react-dom';
import { Layer, Button } from 'grommet';

const modalRoot: HTMLElement = document.getElementById('modal-root') as HTMLElement;

type ModalProps = {
  open: boolean;
  setOpen: () => void;
};

const Modal: React.FC<ModalProps> = ({ setOpen, open, children }) => {
  return open
    ? ReactDOM.createPortal(
        <Layer onEsc={() => setOpen()} onClickOutside={() => setOpen()}>
          {children}
        </Layer>,
        modalRoot
      )
    : null;
};

export default Modal;
