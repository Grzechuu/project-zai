import React from 'react';
import { Button, Menu, Box } from 'grommet';
import { handleLogout } from 'api/security';
import history from 'utils/history';

const Navbar: React.FC = () => {
  return (
    <Box
      // gridArea="header"
      background="brand"
      direction="row"
      align="center"
      justify="between"
      pad={{ horizontal: 'medium', vertical: 'small' }}
    >
      <Button hoverIndicator />
      <Menu
        label="account"
        items={[
          {
            label: 'boards',
            onClick: () => {
              history.push('/boards');
            },
          },
          { label: 'logout', onClick: handleLogout },
        ]}
      />
    </Box>
  );
};

export default Navbar;
