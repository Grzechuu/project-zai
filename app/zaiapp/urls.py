from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls

API_TITLE = 'API Task Manager'
API_DESCRIPTION = 'Task Manager :)'

urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include("apps.routers", namespace="api")),
    path('', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION))
]
