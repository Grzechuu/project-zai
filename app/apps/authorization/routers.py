from rest_framework.routers import SimpleRouter

from .viewsets import AuthenticationViewSet

app_name = "auth"

router = SimpleRouter()
router.register("", AuthenticationViewSet, "auth")

urlpatterns = router.urls
