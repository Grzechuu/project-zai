from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import UserManager
from django.utils.translation import gettext_lazy as _


class Account(AbstractUser):
    email = models.EmailField(unique=True, null=False, blank=False)
    username = None

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ()

    objects = UserManager()

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        verbose_name = _("account")
        verbose_name_plural = _("accounts")
