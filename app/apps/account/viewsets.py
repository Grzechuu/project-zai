from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .serializers import RegisterSerializer, AccountSerializer


class AccountViewSet(GenericViewSet):

    def get_serializer_class(self):
        if self.action == "create":
            return RegisterSerializer
        return AccountSerializer

    def create(self, request: Request) -> Response:
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
