from rest_framework import serializers

from .models import Account
from django.contrib.auth.password_validation import validate_password

from django.utils.translation import gettext_lazy as _


class RegisterSerializer(serializers.ModelSerializer):

    def validate_password(self, value):
        validate_password(value)
        return value

    def save(self, **kwargs):
        email = self.validated_data['email']
        password = self.validated_data['password']
        return Account.objects.create_user(email=email, password=password, **kwargs)

    class Meta:
        model = Account
        fields = ["email", "password"]


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ["id", "email"]
