from rest_framework.routers import SimpleRouter

from .viewsets import AccountViewSet

app_name = "account"

router = SimpleRouter()

router.register("", AccountViewSet, "account")

urlpatterns = router.urls
