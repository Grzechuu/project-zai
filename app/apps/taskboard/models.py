from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class TaskBoard(models.Model):
    title = models.CharField(max_length=1000)
    description = models.TextField()
    
    class Meta:
        verbose_name = _('Task Board')
        verbose_name_plural = _("Task Boards")


class TaskBoardMember(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    taskboard = models.ForeignKey(TaskBoard, on_delete=models.CASCADE, related_name="members")

    class Meta:
        verbose_name = _('Member')
        verbose_name_plural = _("Members")


class Task(models.Model):
    # Base fields
    name = models.CharField(max_length=1000)
    description = models.TextField()
    dateFrom = models.DateTimeField()
    dateTo = models.DateTimeField()
    # Relations
    taskboard = models.ForeignKey(TaskBoard, on_delete=models.PROTECT, related_name="tasks")

    class Meta:
        verbose_name = _('Task')
        verbose_name_plural = _("Tasks")
