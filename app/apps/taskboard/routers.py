from rest_framework_extensions.routers import ExtendedSimpleRouter

from .viewsets import TaskBoardViewSet, TaskViewSet

app_name = "taskbard"

router = ExtendedSimpleRouter()
router.register("", TaskBoardViewSet, "taskboard").register("task", TaskViewSet,
                                                            parents_query_lookups=['taskboard_id'],
                                                            basename="tasks")

urlpatterns = router.urls
