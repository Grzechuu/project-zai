from rest_framework import serializers

from .models import TaskBoard, TaskBoardMember, Task


class TaskSerializer(serializers.ModelSerializer):

    def create(self, validated_data: dict):
        print(validated_data)
        return super().create(validated_data)

    class Meta:
        model = Task
        fields = "__all__"


class TaskBoardSerialzier(serializers.ModelSerializer):

    def create(self, validated_data: dict):
        owner = validated_data.pop("owner")
        taskboard = super().create(validated_data)
        TaskBoardMember.objects.create(taskboard=taskboard, account=owner)
        return taskboard

    class Meta:
        model = TaskBoard
        fields = "__all__"


class TaskBoardListItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskBoard
        fields = ["id", "title", "members"]
