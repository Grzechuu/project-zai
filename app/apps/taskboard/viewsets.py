from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin

from .filters import TaskFilter
from .models import TaskBoard, Task
from .serializers import TaskBoardSerialzier, TaskBoardListItemSerializer, TaskSerializer


class TaskBoardViewSet(ModelViewSet):
    serializer_class = TaskBoardSerialzier
    queryset = TaskBoard.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == "list":
            return TaskBoardListItemSerializer
        return super().get_serializer_class()

    def create(self, request: Request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=user)
            return Response(serializer.data, status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class TaskViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    model = Task
    filter_backends = [DjangoFilterBackend]
    filterset_class = TaskFilter

    # permission_classes = (IsAuthenticated,)

    def get_serializer_context(self, *args, **kwargs):
        context = super().get_serializer_context(*args, **kwargs)
        params = self.get_parents_query_dict()
        if params:
            if params['taskboard_id']:
                context.update({'taskboard': params['taskboard_id']})
        return context
