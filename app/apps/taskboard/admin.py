from django.contrib import admin

from .models import TaskBoard, TaskBoardMember


class TaskBoardAdmin(admin.ModelAdmin):
    """
    Obsługa tablic użytkowników
    """
    pass


class TaskMemberAdmin(admin.ModelAdmin):
    pass


admin.site.register(TaskBoard, TaskBoardAdmin)
admin.site.register(TaskBoardMember, TaskMemberAdmin)
