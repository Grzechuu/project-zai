from django.db.models import Q
from django_filters import rest_framework as filters


class TaskFilter(filters.FilterSet):
    day = filters.CharFilter(method="get_day_in")

    month = filters.CharFilter(method="get_month_in")
    year = filters.CharFilter(method="get_year_in")

    def get_day_in(self, queryset, name, value):
        return queryset.filter(Q(dateFrom__day__lte=value) & Q(dateTo__day__gte=value))

    def get_month_in(self, queryset, name, value):
        return queryset.filter(
            Q(dateFrom__month__lte=value) & Q(dateTo__month__gte=value))

    def get_year_in(self, queryset, name, value):
        return queryset.filter(
            Q(dateFrom__year__lte=value) & Q(dateTo__year__gte=value))
