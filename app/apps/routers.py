from django.urls import path, include

app_name = "api"

urlpatterns = [
    path("account/", include("apps.account.routers", namespace="account")),
    path("auth/", include("apps.authorization.routers", namespace="auth")),
    path("taskboard/", include("apps.taskboard.routers", namespace="taskboard"))
]
