import factory
from factory import django

from apps.account.models import Account


class AccountFactory(django.DjangoModelFactory):
    """
    Account factory
    """

    class Meta:
        model = Account

    email = factory.Faker("email")

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Get account manager
        manager = cls._get_manager(model_class)
        # create user from manager method
        return manager.create_user(*args, **kwargs)
