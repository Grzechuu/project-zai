import pytest


@pytest.mark.django_db
class TestUserAuthentication:

    def test_user_should_given_token_after_success_authorization(self, client, account_factory):
        data = {
            "email": "simple@test.pl",
            "password": "Secret@123"
        }
        account_factory(email=data['email'], password=data["password"])

        response = client.post("/api/auth/", data)

        assert response.status_code == 200
        assert "token" in response.data

    def test_user_typed_wrong_password_and_should_given_error_message_about_wrong_authorization(self, client,
                                                                                                account_factory):
        data = {
            "email": "simple@test.pl",
            "password": "Secret@123"
        }
        account_factory(email=data['email'], password="AnotherPassword")

        response = client.post("/api/auth/", data)

        assert response.status_code == 400
        assert "errors" in response.data

    def test_user_typed_wrong_email_and_should_given_error_message_about_wrong_authorization(self, client,
                                                                                             account_factory):
        data = {
            "email": "simple@test.pl",
            "password": "Secret@123"
        }
        account_factory(email="wrong@email.com", password=data['password'])

        response = client.post("/api/auth/", data)

        assert response.status_code == 400
        assert "errors" in response.data
