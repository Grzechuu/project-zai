import pytest
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory

from apps.account.models import Account
from apps.account.viewsets import AccountViewSet


@pytest.mark.django_db
class TestRegisterUser:

    def test_anonymous_should_can_register(self, api_rf: APIRequestFactory):
        view = AccountViewSet.as_view({"post": "create"})
        data = {
            "email": "test@test.pl",
            "password": "Secret@123"
        }
        request = api_rf.post(path="/", data=data)
        response: Response = view(request)

        assert response.status_code == 201
        account = Account.objects.first()
        assert account is not None
        assert account.email == data['email']

    @pytest.mark.parametrize("password", ["bad", "toobaad"])
    def test_password_for_new_account_should_be_complex(self, api_rf: APIRequestFactory, password):
        view = AccountViewSet.as_view({"post": "create"})
        data = {
            "email": "test@test.pl",
            "password": password
        }

        request = api_rf.post(path="/", data=data)
        response: Response = view(request)

        assert response.status_code == 400
        assert "password" in response.data
