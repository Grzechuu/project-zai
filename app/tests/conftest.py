import pytest
from pytest_factoryboy import register
from rest_framework.test import APIRequestFactory

from .factories import AccountFactory

register(AccountFactory)


@pytest.fixture(scope="function")
def user(account_factory):
    return account_factory(email="test@test.pl", password="Secret!123")


@pytest.fixture(scope="function")
def api_rf():
    return APIRequestFactory()
