import pytest
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory, force_authenticate

from apps.taskboard.models import TaskBoard, TaskBoardMember
from apps.taskboard.serializers import TaskBoardSerialzier
from apps.taskboard.viewsets import TaskBoardViewSet


@pytest.mark.django_db
class TestCreateTaskboard:

    def test_anonymous_user_cant_create_task(self, api_rf: APIRequestFactory):
        """
        Niezalogowany użytkownik powinien dostać informację o wymaganiu zalogoawnia, aby wykonać akcję utworzenia nowej tablicy
        """
        view = TaskBoardViewSet.as_view({"post": "create"})
        request = api_rf.post("/test")

        response: Response = view(request)

        assert response.status_code == 401

    def test_user_can_create_taskboard(self, api_rf: APIRequestFactory, user):
        """
        Zalogowany użytkownik powinien utworzyć nową tablicę.
        Na bazie danych powinny utworzyć się dwa obiekty:
        * Taskboard
        * TaskboardMember powiązany relacją z Taskboard oraz użytkownikiem
        """
        view = TaskBoardViewSet.as_view({'post': 'create'})
        data = {
            "title": "Nowa Tablica",
            "description": "test"
        }
        request = api_rf.post("/test", data=data)
        force_authenticate(request, user=user)

        response: Response = view(request)

        assert response.status_code == 201
        taskboard = TaskBoard.objects.first()
        taskboard_member = TaskBoardMember.objects.get(taskboard=taskboard)
        assert response.data == TaskBoardSerialzier(taskboard).data
        assert taskboard_member.account == user
