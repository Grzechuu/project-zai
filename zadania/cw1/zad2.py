firstname = 'Dariusz'
surname = 'Kowalczyk'

lorem_ipsum = "Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu, zawierających fragmenty Lorem Ipsum, a ostatnio z zawierającym różne wersje Lorem Ipsum oprogramowaniem przeznaczonym do realizacji druków na komputerach osobistych, jak Aldus PageMaker"

first_char = firstname[2]
second_char = surname[1]

liczba_liter1 = sum([1 for char in lorem_ipsum if char == first_char])
liczba_liter2 = sum([1 for char in lorem_ipsum if char == second_char])

print("W tekście jest {liczba_liter1} liter '{first_char}' oraz {liczba_liter2} liter '{second_char}'".format(liczba_liter1=liczba_liter1,liczba_liter2=liczba_liter2,first_char=first_char,second_char=second_char))
