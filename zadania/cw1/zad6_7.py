list_1 = [i for i in range(10)]
list_2 = list_1[5:]
del list_1[5:]
print(list_1, list_2)

new_list = list_1 + list_2
print(new_list)
new_list.insert(0,0)
print(new_list)
copy_list = new_list.copy()
print(sorted(copy_list, reverse=True))
