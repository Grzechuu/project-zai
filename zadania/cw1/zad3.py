
from datetime import datetime

print('Lubię {zmienna}'.format(zmienna="placki"))
print('Liczba {:-d}'.format(586))
print('Liczba {:+d}'.format(18))
print('{:%Y-%m-%d %H:%M}'.format(datetime.now()))
print('{:{prec}} = {:{prec}}'.format('PI', 3.141592653589793238, prec='.2'))